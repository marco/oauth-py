Github OAuth

How-to setup:
https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/

Developer guide
https://developer.github.com/enterprise/2.13/apps/building-oauth-apps/authorizing-oauth-apps/

GitHub v3 API:
https://developer.github.com/enterprise/2.13/v3

Use Python requests-oauth:
https://requests-oauthlib.readthedocs.io/en/latest/oauth2_workflow.html


Example Redirect URI:

https://github.com/oauth/?code=3b67281bd07f2dab54e7&state=PWRAlN993OdGoW3Dhp8kJ6snAR8ggM
